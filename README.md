# Fizzbuzz on Steroids

We're going to do fizzbuzz, a quintosntial exercise of programing. We'll add testing to it and a CI pipeline to have test around.

We'll start by make some unit tests, and employ Test Driven Development. Add these test to our pipeline. And then just focus on the code :) 

Things we'll cover:

- Git and Markdown
- Python Funcitonal Programming
- TDD - test driven development
- CI - Continuous integration
- Mini scrum project (without the board) - Sprints, users stories and DoD, acceptance criterea

### Definition of Done

`Todo later` - What things are do we need to check that are communual to all user stories?

- all acceptance criterea passed

### User stories

#Game rules
if it is a multiple of 3 --> fizz
if a multiple of 5 --> buzz
multiple of 3 and 5 --> fizzbuzz

**Core game logix user stories**
**User Story 1** As a player, when we give the game a multiple of 3 the game should output fizz, so that it follows the game logic.

**User Story 2** As a player, when we give the game a multiple of 5 the game should output buzz, so that it follows the game logic.

**User Story 3** As a player, when we give the game a multiple of 3 and 5 the game should output fizzbuzz, so that it follows the game logic.

**Extra users stories**
To come later

### psudo code / plan